# TorusCycle

An incomplete Tron WebGL clone. [Preview it.][preview]

[![TorusCycle screenshot](screenshot.jpg)][preview]

## Usage

Start the server and watch for changes:

    npm start

## License

ISC

[preview]: http://nechifor.net/torus-cycle
